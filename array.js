var names = ['aamir', 'lubna', 'sana'];
var age = [28, 32, 45];

console.log(names[1] + age[1])

var aamir = ["Male", 31, "SystemEngineer", "cricket", "false"]
console.log(aamir[4]);

var aamira = prompt('Likho');
console.log(aamira);

aamir.push('blue');   //pushes to an end of any array.
aamir.unshift('Mr. '); // add to the beginnig of an array.
aamir.pop();           //  remooves last element from an array.
aamir.shift();         // removes first element of an array.

console.log(aamir);

myno = aamir.indexOf("cricket");   //provides index no of the element menetioned inside an array.
console.log(myno);

alert(aamir.indexOf("false"));

if (aamir.indexOf('SystemEngineer') === -1) {
    console.log('Match not found');
} else {
    console.log('Match found');
}